program Project1;

{$APPTYPE CONSOLE}

{$R *.res}

uses Windows,Messages,SysUtils,Variants,Classes,Math,StrUtils;

procedure hostsSites(_in:string);
var content,cToWrite:tstringlist;
toWrite,line:string;
chr:char;
begin
  writeln('Processing '+_in);
  cToWrite := TStringList.Create; content := TStringList.Create; content.LoadFromFile(_in);
  cToWrite.Sorted:=true; cToWrite.Duplicates:=dupIgnore;          // Set type of TStringList to sorted and ignore duplicates
  for line in content do                                          // For each line in TStringList in input file
  begin                                                           // BEGIN BLEAT6
    if ((length(line)>0) and (line[1]<>'#')) then                 // If length>0 and not starts with # (comment)
    begin                                                         // Begin trimming from comments
      toWrite:='';                                                // Empty line from previous text
      for chr in line do                                          // For each char in line in TStringList in input file
        if chr='#' then break                                     // If # then end line
        else toWrite:=toWrite+chr;                                // Else add char to line
        toWrite:=trim(toWrite);                                   // Trim line
        toWrite:=toWrite.Replace(#9,'');                          // Trim line from tab
        toWrite:=toWrite.Replace('http://','');                   // Trim line
        if toWrite.Contains(' www.') or toWrite.Contains(#9'www.') then
          toWrite:=toWrite.Replace('www.','');                    // Trim line
        if (length(toWrite)>0) and toWrite.Contains(' ') then     // If length>0 and line contains space then
          cToWrite.Add(toWrite.Split([' '])[toWrite.CountChar(' ')]);// Add to TStringList last string of splited line
    end
  end;
  cToWrite.SaveToFile(_in+'Sites');
end;

procedure Merge(arg:array of string);
var list:array[0..1] of tstringlist;
s:string;
begin
  list[0]:= TStringList.Create;
  list[1]:= TStringList.Create;
  list[0].Sorted := True;
  list[0].Duplicates := dupIgnore;
  for s in arg do
  begin
    writeln('Processing '+s);
    list[0].LoadFromFile(s+'sites');
    list[1].AddStrings(list[0]);
  end;
  list[1].SaveToFile('TheHostsSites');
end;

procedure addlocalhost(adress:string);
var Slist:tstringlist;
s:string;
i:integer;
begin
  Slist:= TStringList.Create;
  Slist.LoadFromFile('TheHostsTrimmed');

  for i:=0 to slist.Count-1 do
  slist[i]:= adress+' '+slist[i];
  slist.Insert(0,'# This is Hosts file. Do not edit! Contains '+(slist.Count).ToString+' entries. Updated '+DateTimeToStr(Now)+' (c) yrambler2001');
  slist.Insert(1,'');
  slist.Insert(2,'127.0.0.1 localhost');
  slist.SaveToFile('TheHosts');
end;

procedure hostsTrim();
var Slist:tstringlist;
begin
  Slist:= TStringList.Create;
  Slist.LoadFromFile('TheHostsSites');
  slist.Delete(slist.IndexOf('localhost'));
  slist.SaveToFile('TheHostsTrimmed');
end;

procedure ConsoleLine(s,char_:string = '=');
var i:integer;
begin
  for i := 1 to ceil(40-(length(s)/2)) do
  write(char_);
  write(s);
  if length(s) mod 2 = 0 then
  for i := 1 to ceil(40-(length(s)/2)) do
  write(char_)
  else
  for i := 2 to ceil(40-(length(s)/2)) do
  write(char_);
end;


var s:string; i:integer;
    hosts:array of string;
begin
  try
  if ParamCount=0 then
    begin writeln('0 args'); Exit end;

  for i:=1 to ParamCount do                                       // for each parameter test if file exists
    if FileExists(ParamStr(i)) then
    begin
      setlength(hosts,length(hosts)+1);
      hosts[length(hosts)-1]:=paramstr(i);                        // if exists then add to array
    end;

  if length(hosts)=0 then exit;

  consoleline('Extracting sites from hosts');
  for s in hosts do
  hostsSites(s);
  consoleline('Merging sites to one hosts');
  merge(hosts);
  consoleline('Trimming hosts');
  hostsTrim;
  consoleline('Adding 0.0.0.0');
  addlocalhost('0.0.0.0');
 consoleline('Hosts created');



  readln;
  exit;
  write('ok'); readln;


  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
